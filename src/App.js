import React, {useState, useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import firebase from './components/config/firebase';

//Import de componentes
import Header from './components/Header';
import AddLab from './components/AddLab';
import Horarios from './components/Horarios';
import AddHorario from './components/AddHorario';
import Labs from './components/Labs';
import EditarHorario from './components/EditarHorario';
import EditarLab from './components/EditarLab';
import DetalleHorario from './components/DetalleHorario';
import DetalleLab from './components/DetalleLab';
import Marcador from './components/Marcador';
import Ar from './components/Ar';
import Login from './components/Auth/Login';
import Relleno from './components/Relleno'

function App() {
  //En el brazo de pruebas
  
  const [laboratorios, setLaboratorios] = useState([]);
  const [horarios, setHorarios] = useState([]);
  const [recargar, setRecargar] = useState(true);
  const [autenticado, setAutenticado] = useState(false);

  useEffect(() => {
    if (recargar) {
      firebase.firestore().collection('laboratorio').onSnapshot((snapshot)=>{
        const datos = snapshot.docs.map((dato)=>({
          id: dato.id,
          ...dato.data()
        }))
        setLaboratorios(datos);
      });
      firebase.firestore().collection('horario').onSnapshot((snapshot)=>{
        const datos = snapshot.docs.map((dato)=>({
          id: dato.id,
          ...dato.data()
        }))
        setHorarios(datos);
      });
    }
    //Cambiar a false la recarga, para que no se este consultando a la API a cada momento
    setRecargar(false);
    //Watcher para saber si el usuario esta logeado o no
    firebase.auth().onAuthStateChanged((user)=>{
      if (user) {
          //El state se pone en true si el usuario esta logeado
          return setAutenticado(true);
      }else{
          //El state se pone en false si el usuario esta logeado
          return setAutenticado(false);
      }
    })
    
  }, [recargar])

  return (
    <Router>
      <Header />
      <main className="container mb-5 ">
        <Switch>
          {/*Ruta login*/}
          <Route exact path="/" render={()=>(
            <Relleno/>
          )}/>
          {/*Ruta login*/}
          <Route exact path="/login" render={()=>(
            <Login recargar={setRecargar} auth={autenticado}/>
          )}/>
          {/*Ruta 1*/}
          <Route exact path="/nuevo-marcador" render={()=>(
            <Marcador auth={autenticado}/>
          )} />
          {/*Ruta 2*/}
          <Route exact path="/public-app" render={()=>( 
            <Ar datos={horarios} />
          )}/>
          {/*Ruta 3*/}
          <Route exact path="/laboratorios" render={()=>(
            <Labs laboratorios={laboratorios} recargar={setRecargar} auth={autenticado}/>
          )} />
          {/*Ruta 4*/}
          <Route exact path="/nuevo-laboratorio"  render={()=>(
            <AddLab recargar={setRecargar} auth={autenticado}/>
          )} />
          {/*Ruta 5*/}
          <Route exact path="/horarios" render={()=>(
            <Horarios horarios={horarios} recargar={setRecargar} auth={autenticado}/>
          )} />
          {/*Ruta 6*/}
          <Route exact path="/nuevo-horario" render={()=>(
            <AddHorario datos={laboratorios} recargar={setRecargar} auth={autenticado}/>
          )} />
          {/*Ruta 7*/}
          <Route exact path="/horarios/editar/:id" render={props=> {
            //Tomando el id del horario
            const idHorario = props.match.params.id;
            //horario que se pasa al state 
            const horario = horarios.filter(horario => horario.id === idHorario);
            return (
              <EditarHorario horario={horario[0]} datos={laboratorios}/>
            )
          }}/>
          {/*Ruta 8*/}
          <Route exact path="/laboratorios/editar/:id" render={props=>{
            //Tomando el id del lab
            const idLab = props.match.params.id;
            //lab que se pasa al state 
            const lab = laboratorios.filter(lab => lab.id === idLab);
            return (
              <EditarLab lab={lab[0]}/>
            )
          }} />
          {/*Ruta 9*/}
          <Route exact path="/horarios/detalle/:id" render={props=> {
            //Tomando el id del horario
            const idHorario = props.match.params.id;
            //horario que se pasa al state 
            const horario = horarios.filter(horario => horario.id === idHorario);
            return (
              <DetalleHorario horario={horario[0]}/>
            )
          }} /> 
          {/*Ruta 10*/}
          <Route exact path="/laboratorios/detalle/:id" render={props=> {
            //Tomando el id del horario
            const idLab = props.match.params.id;
            //lab que se pasa al state 
            const lab = laboratorios.filter(lab => lab.id === idLab);
            return (
              <DetalleLab laboratorio={lab[0]}/>
            )
          }}/>

        </Switch>
      </main>
    </Router>
  );
}

export default App;
