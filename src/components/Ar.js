import React, { Component } from 'react';
import { AFrameRenderer, Marker } from 'react-web-ar';
import firebase from './config/firebase';

export default class Ar extends Component {
	state = {
		ini: 'hola',
		val: true,
		val2: true,
		val3:true,
		val4:true,
		resultado1: [],
		resultado2: [],
		resultado3: [],
		resultado4: [],
		respuesta: '',
		respuesta2: '',
		respuesta3: '',
		respuesta4: ''
  };
  
	laboratorio1 = () => {

		//hacer la consulta
		firebase.firestore().collection('horario').where('laboratorio', '==', 'Laboratorio 1').onSnapshot((snapshot) => {
			const datos = snapshot.docs.map((dato) => ({
				id: dato.id,
				...dato.data()
			}));
			this.setState({
				resultado1: datos
			});
			//console.log(this.state.resultado1);
			var acumulador = '';
			for (var x = 0; x < this.state.resultado1.length; x++) {
				acumulador =
					acumulador +
					this.state.resultado1[x].materia +
					' :' +
					this.state.resultado1[x].nombreDocente +
					'\n' +
					this.state.resultado1[x].horaini +
					' ' +
					this.state.resultado1[x].horafin +
					'\n' +
					this.state.resultado1[x].dia +
					'\n' +
					this.state.resultado1[x].laboratorio + '\n--------------------------\n';
			}
			this.setState({
				respuesta: acumulador
			});
		});
	};
	laboratorio2 = () => {
		//hacer la consulta
		firebase.firestore().collection('horario').where('laboratorio', '==', 'Laboratorio 2').onSnapshot((snapshot) => {
			const datos = snapshot.docs.map((dato) => ({
				id: dato.id,
				...dato.data()
			}));
			this.setState({
				resultado2: datos
			});
			//console.log(this.state.resultado2);

			var acumulador1 = '';
			for (var x = 0; x < this.state.resultado2.length; x++) {
				acumulador1 =
					acumulador1 +
					this.state.resultado2[x].materia +
					' :' +
					this.state.resultado2[x].nombreDocente +
					'\n' +
					this.state.resultado2[x].horaini +
					' ' +
					this.state.resultado2[x].horafin +
					'\n' +
					this.state.resultado2[x].dia +
					'\n' +
					this.state.resultado2[x].laboratorio + '\n--------------------------\n';
			}
			this.setState({
				respuesta2: acumulador1
			});
		});
	};
	laboratorio3 = () => {
		//hacer la consulta
		firebase.firestore().collection('horario').where('laboratorio', '==', 'Laboratorio 3').onSnapshot((snapshot) => {
			const datos = snapshot.docs.map((dato) => ({
				id: dato.id,
				...dato.data()
			}));
			this.setState({
				resultado3: datos
			});
			//console.log(this.state.resultado3);

			var acumulador1 = '';
			for (var x = 0; x < this.state.resultado3.length; x++) {
				acumulador1 =
					acumulador1 +
					this.state.resultado3[x].materia +
					' :' +
					this.state.resultado3[x].nombreDocente +
					'\n' +
					this.state.resultado3[x].horaini +
					' ' +
					this.state.resultado3[x].horafin +
					'\n' +
					this.state.resultado3[x].dia +
					'\n' +
					this.state.resultado3[x].laboratorio + '\n--------------------------\n';
			}
			this.setState({
				respuesta3: acumulador1
			});
		});
	};
	laboratorio4 = () => {
		//hacer la consulta
		firebase.firestore().collection('horario').where('laboratorio', '==', 'Laboratorio 4').onSnapshot((snapshot) => {
			const datos = snapshot.docs.map((dato) => ({
				id: dato.id,
				...dato.data()
			}));
			this.setState({
				resultado4: datos
			});
			//console.log(this.state.resultado4);

			var acumulador1 = '';
			for (var x = 0; x < this.state.resultado4.length; x++) {
				acumulador1 =
					acumulador1 +
					this.state.resultado4[x].materia +
					' :' +
					this.state.resultado4[x].nombreDocente +
					'\n' +
					this.state.resultado4[x].horaini +
					' ' +
					this.state.resultado4[x].horafin +
					'\n' +
					this.state.resultado4[x].dia +
					'\n' +
					this.state.resultado4[x].laboratorio + '\n--------------------------\n';
			}
			this.setState({
				respuesta4: acumulador1
			});
		});
	};

	render() {
    if (this.state.val) {
      this.laboratorio1();
      console.log('consulta lab 1');
      this.setState({
        val:false
      });
    }
    if (this.state.val2) {
      this.laboratorio2();
      console.log('consulta lab 2');
      this.setState({
        val2:false
      });
    }
    if (this.state.val3) {
      this.laboratorio3();
      console.log('consulta lab 3');
      this.setState({
        val3:false
      });
    }
    if (this.state.val4) {
      this.laboratorio4();
      console.log('consulta lab 4');
      this.setState({
        val4:false
      });
    }
		return (
			<AFrameRenderer arToolKit={{ sourceType: 'webcam' }}>
				{/* Se llama al archivo.patt que esta en github ya que no se lo puede llamar de forma local por el problema de fuera de ememoria del ar.js  */}
				<Marker
					parameters={{
						preset: 'pattern',
						type: 'pattern',
						url: 'https://raw.githubusercontent.com/JurgenHQ/Archivos-patt/master/LAB1.patt'
					}}
				>
					<a-text
						value={this.state.respuesta}
						rotation="-100 0 0"
						color="#000EFF"
						height="2.5"
						width="2.5"
						position="-0.5 0.1 0"
					/>
				</Marker>
				<Marker
					parameters={{
						preset: 'pattern',
						type: 'pattern',
						url: 'https://raw.githubusercontent.com/JurgenHQ/Archivos-patt/master/LAB2.patt'
					}}
				>
					<a-text
						value={this.state.respuesta2}
						rotation="-100 0 0"
						color="#000EFF"
						height="2.5"
						width="2.5"
						position="-0.5 0.1 0"
					/>
				</Marker>
				<Marker
					parameters={{
						preset: 'pattern',
						type: 'pattern',
						url: 'https://raw.githubusercontent.com/JurgenHQ/Archivos-patt/master/LAB3.patt'
					}}
				>
					<a-text
						value={this.state.respuesta3}
						rotation="-100 0 0"
						color="#000EFF"
						height="2.5"
						width="2.5"
						position="-0.5 0.1 0"
					/>  
				</Marker>
				<Marker
					parameters={{
						preset: 'pattern',
						type: 'pattern',
						url: 'https://raw.githubusercontent.com/JurgenHQ/Archivos-patt/master/LAB4.patt'
					}}
				>
					<a-text
						value={this.state.respuesta4}
						rotation="-100 0 0"
						color="#000EFF"
						height="2.5"
						width="2.5"
						position="-0.5 0.1 0"
					/>
				</Marker>
			</AFrameRenderer>
		);
	}
}
