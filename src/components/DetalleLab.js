import React from 'react';
import { Link } from 'react-router-dom';


const DetalleLab = ({laboratorio}) => {
    return (
        <div className="container">
            <br/><br/>
            <div className="card mt-5">
                <h4 className="card-header">{laboratorio.nombreLab}</h4>
                <div className="card-body">
                    <h5 className="card-title">Descripción del laboratorio</h5>
                    <p className="card-text">{laboratorio.descripcionLab}</p>
                    {/* 
                    <h5 className="card-title">Archivo .patt</h5>
                    <p className="card-text">{laboratorio.patt}</p>
                    */}
                    <br/>
                    <Link to="/laboratorios" className="btn btn-primary">Regresar</Link>
                </div>
            </div>
            <br/><br/><br/><br/><br/><br/>
        </div>
    )
}
export default DetalleLab;