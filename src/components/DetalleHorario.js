import React from 'react';
import { Link } from 'react-router-dom';

const DetalleHorario = ({horario}) => {
    return (
        <div className="container">
            <div className="card mt-5">
                <h5 className="card-header">{horario.laboratorio}</h5>
                <div className="card-body">
                    <h5 className="card-title">Nombre del Docente:</h5>
                    <p className="card-text">{horario.nombreDocente}</p>
                    <h5 className="card-title">Materia que se dicta:</h5>
                    <p className="card-text">{horario.materia}</p>
                    <h5 className="card-title">Día:</h5>
                    <p className="card-text">{horario.dia}</p>
                    <h5 className="card-title">Horario:</h5>
                    <p className="card-text">Desde: {horario.horaini}h</p>
                    <p className="card-text">Hasta: {horario.horafin}h</p>
                    <Link to="/horarios" className="btn btn-primary mt-4 ">Regresar</Link>
                </div>
            </div>
        </div>
    )
}

export default DetalleHorario;
