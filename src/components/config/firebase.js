import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

var firebaseConfig = {
    apiKey: "AIzaSyD--r9-hVjPvUzBf3HppWrS65Qq3UqyLNU",
    authDomain: "appgya-2f776.firebaseapp.com",
    databaseURL: "https://appgya-2f776.firebaseio.com",
    projectId: "appgya-2f776",
    storageBucket: "appgya-2f776.appspot.com",
    messagingSenderId: "987624535087",
    appId: "1:987624535087:web:6886347b04b3072e9442da",
    measurementId: "G-4XNS3QBZSV"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
