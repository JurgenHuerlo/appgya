import React from 'react';
import LabLista from './LabLista';

const Labs = ({laboratorios, recargar, auth}) => {
    return (
        <div className="jumbotron mt-5">
            {auth ? (
            <div>
                <legend className="mb-4 text-center text-uppercase font-weight-bold" >Listado Laboratorios</legend>
                <ul className="list-group mt-5">
                    {laboratorios.map(laboratorio => (
                        <LabLista key={laboratorio.id} laboratorio={laboratorio} recargar={recargar} />
                    ))}
                </ul>
            </div>
            ): <h1 className="alert alert-danger p3 my-5 text-center text-uppercase font-weight-bold">Hey humano no tienes permiso para ver esto.</h1>}
        </div>
    )
}
export default Labs
