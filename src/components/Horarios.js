import React from 'react';
import HorarioLista from './HorarioLista';

export default function Horarios({horarios, recargar, auth}) {
    return (
        <div className="jumbotron mt-5">
            {auth ?(
            <div>
                <legend className="mb-4 text-center text-uppercase font-weight-bold" >Listado Horarios</legend>
                <ul className="list-group mt-5">
                    {horarios.map(horario => (
                        <HorarioLista key={horario.id} horario={horario} recargar={recargar} />
                    ))}
                </ul>
            </div>
            ): <h1 className="alert alert-danger p3 my-5 text-center text-uppercase font-weight-bold">Hey humano no tienes permiso para ver esto.</h1>}
        </div>
    )
}
