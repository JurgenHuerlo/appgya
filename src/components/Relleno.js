import React,{useState} from 'react';
import {Link} from 'react-router-dom'
import imageJS from '../imageJS.gif';
import imageWork from '../imgWork.jpg';
import firebase from './config/firebase';

function Relleno () {
    const [usuarioAutenticado, setUsuarioAutenticado] = useState(false);
    firebase.auth().onAuthStateChanged((user)=>{
        if (user) {
            //El state se pone en true si el usuario esta logeado
            return setUsuarioAutenticado(true);
        }else{
            //El state se pone en false si el usuario esta logeado
            return setUsuarioAutenticado(false);
        }
    })
    return (
        <div className="row justify-content-center mt-3 mb-3">
            {usuarioAutenticado ? (
                <div className="col-md-5 mb-2">
                    <div className="card mt-5 mb-2">
                        <img src={imageWork} className="card-img-top" alt="Gif JavaScript"></img>
                        <div className="card-body mb-2 mt-3">
                            <h5 className="card-title">Bienvenido de vuelta</h5>
                            <p className="card-text">Empieza a trabajar y administrar</p>
                            <Link to='/laboratorios' className="btn btn-primary btn-block">A trabajar</Link>
                        </div>
                    </div>
                </div>
            ):(
                <div className="col-md-5 mb-2">
                    <div className="card mt-5 mb-2">
                        <img src={imageJS} className="card-img-top" alt="Gif JavaScript"></img>
                        <div className="card-body mb-2 mt-3">
                            <h5 className="card-title">Gracias por visitar la Página</h5>
                            <p className="card-text">Puedes loguearte dado click en el botón o ingresar directamente a la App</p>
                            <Link to='/login' className="btn btn-primary btn-block">Inicia Sesión</Link>

                        </div>
                    </div>
                </div>
            )}
        </div> 
    )
}

export default Relleno;
